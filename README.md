# [Snake ThreeJS](https://snake-threejs.netlify.app/)

> Simple 2.5D snake written with react-three-fiber and threejs.

## Getting Started

1. `npm install`
2. `npm start`

## External Resources

- [Source](https://gitlab.com/nate-wilkins/snake-25)
- [CanvasTexture](https://dustinpfister.github.io/2018/04/17/threejs-canvas-texture/)
- [MeshStandardMaterial]https://threejs.org/docs/#api/en/materials/MeshStandardMaterial)
