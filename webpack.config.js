import path from 'path';
import process from 'process';
import {
  HotModuleReplacementPlugin,
  NoEmitOnErrorsPlugin,
  SourceMapDevToolPlugin
} from 'webpack';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import CircularDependencyPlugin from 'circular-dependency-plugin';

const isProd = process.env.NODE_ENV === 'production';

module.exports = {
  entry: path.resolve(__dirname, './src/index.jsx'),

  output: {
    path: path.resolve(__dirname, './dist'),
    filename: "bundle.js",
  },

  devServer: {
    port: 4550,

    // use index.html
    historyApiFallback: {
      disableDotRule: true,
      index: '/index.html',
      rewrites: [],
    },

    // compression
    compress: true,
    // enables webpack's HMR
    hot: true,
    // to fix "invalid host header" error
    disableHostCheck: true,
    // to fix "No 'Access-Control-Allow-Origin' header"
    // error for HMR websocket connection
    headers: { },
    stats: {
      colors: true,
      cachedAssets: false,
    },
  },

  plugins: [
    new HtmlWebpackPlugin({
      template: path.join(__dirname, 'src', 'index.html'),
      filename: "./index.html"
    }),
    new CircularDependencyPlugin({
      // Flow doesn't care about circular dependencies
      // files .flow should only contain flow types
      // https://github.com/aackerman/circular-dependency-plugin/issues/31
      exclude: /node_modules/,
      failOnError: true,
    }),
    new SourceMapDevToolPlugin({
      filename: "[file].map"
    }),
    // enable HMR globally
    new HotModuleReplacementPlugin(),
    new NoEmitOnErrorsPlugin(),
  ],

  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [],
            plugins: [],
          },
        },
      },
      {
        test: /\.(css)$/,
        use: [
          "style-loader",
          {
            loader: 'css-loader',
            options: {
              importLoaders: 1,
              modules: true
            }
          }
        ],
        exclude: /\.?global\.css$/
      },
      {
        test: /\.(css)$/,
        use: [
          "style-loader",
          "css-loader",
        ],
        include: /\.?global\.css$/
      },
      {
        test: /\.(jpg|jpeg|png|gif|mp3|svg|gltf|fbx)$/,
        use: ['file-loader']
      }
    ],
  },

  resolve: {
    modules: ['node_modules'],
    extensions: ['*', '.js', '.jsx']
  },

  mode: isProd ? 'production' : 'development',
  devtool: isProd ? 'source-map' : 'inline-source-map'
};
