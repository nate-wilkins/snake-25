module.exports = {
  parserOptions: {
    ecmaVersion: 2020,
    sourceType: 'module',
    ecmaFeatures: {
      impliedStrict: true,
      jsx: true
    },
  },
  env: {
    browser: true,
    node: true,
    commonjs: true,
    es6: true
  },
  plugins: [
    "react"
  ],
  extends: [
    'eslint:recommended',
    "plugin:react/recommended"
  ],
  rules: {
    'no-console': ['error', { allow: ['warn', 'error'] }],
    'no-unused-vars': [
      'off',
      { ignoreRestSiblings: true, argsIgnorePattern: '^_' },
    ]
  },
  settings: {},
};
