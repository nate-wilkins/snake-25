import React from 'react';
import { useSettingsProvider } from '../hooks/useSettingsProvider';

export const SceneBackdrop = function SceneBackdrop({ children }) {
  const settings = useSettingsProvider();
  const [
    themeBackgroundColor,
    _setThemeBackgroundColor,
  ] = settings.themeBackgroundColor;

  return (
    <div
      style={{
        backgroundColor: themeBackgroundColor,
        minHeight: '100%',
      }}
    >
      {children}
    </div>
  );
};
