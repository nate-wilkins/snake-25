import React from 'react';
import { Typography } from '@material-ui/core';

import { MenuItem } from '../components/MenuItem';
import { useSettingsProvider } from '../hooks/useSettingsProvider';

// eslint-disable-next-line
import styles from './Scene.css';

export const SettingsScene = function SettingsScene({
  onClickBack,
  onClickTheme,
  onClickPerformance,
}) {
  const settings = useSettingsProvider();
  const [arenaWidth, setArenaWidth] = settings.arenaWidth;
  const [arenaHeight, setArenaHeight] = settings.arenaHeight;

  return (
    <div className={styles.centerMenuLayout}>
      <div className={styles.menuLayout}>
        <Typography variant="h1" gutterBottom>
          Snake!
        </Typography>

        <MenuItem isLink onClick={onClickTheme}>
          Theme
        </MenuItem>

        <MenuItem isLink onClick={onClickPerformance}>
          Performance
        </MenuItem>

        <div style={{ display: 'flex', flexDirection: 'column' }}>
          <div className={styles.menuItemGroup}>
            <MenuItem>Width:</MenuItem>
            <input
              style={{ width: '2.7em', marginLeft: '.7em' }}
              type="number"
              value={arenaWidth}
              min="8"
              max="30"
              onChange={event => setArenaWidth(event.target.value)}
            />
          </div>
          <div className={styles.menuItemGroup}>
            <MenuItem>Height:</MenuItem>
            <input
              style={{ width: '2.7em', marginLeft: '.7em' }}
              type="number"
              value={arenaHeight}
              min="8"
              max="30"
              onChange={event => setArenaHeight(event.target.value)}
            />
          </div>
        </div>

        <MenuItem isLink onClick={onClickBack}>
          Back
        </MenuItem>
      </div>
    </div>
  );
};
