import React, { Suspense } from 'react';

import { GameProvider } from './../providers/GameProvider';
import { Canvas } from './../components/Canvas';
import { Arena } from './../components/Arena';
import { GameHud } from './../components/GameHud';
import {
  useSettingsProvider,
  THEME_LOOKUP,
} from './../hooks/useSettingsProvider';

export const GameScene = function GameScene({ onReplay }) {
  const settings = useSettingsProvider();
  const [arenaWidth, _setArenaWidth] = settings.arenaWidth;
  const [arenaHeight, _setArenaHeight] = settings.arenaHeight;
  const [
    themeBackgroundColor,
    _setThemeBackgroundColor,
  ] = settings.themeBackgroundColor;

  return (
    <GameProvider>
      <Suspense fallback={() => <>Loading...</>}>
        <Canvas
          style={{ width: '100vw', height: '100vh' }}
          clearColor={themeBackgroundColor}
        >
          <Arena width={arenaWidth} height={arenaHeight} />
        </Canvas>
        <GameHud onReplay={onReplay} />
      </Suspense>
    </GameProvider>
  );
};
