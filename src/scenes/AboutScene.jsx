import React from 'react';
import { Typography, Link } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import TwitterIcon from '@material-ui/icons/Twitter';

import { MenuItem } from '../components/MenuItem';
import { useSettingsProvider } from '../hooks/useSettingsProvider';

// eslint-disable-next-line
import styles from './Scene.css';

const Code = function Code({ children }) {
  const settings = useSettingsProvider();
  const [
    themeFontHoverColor,
    _setThemeFontHoverColor,
  ] = settings.themeFontHoverColor;

  return (
    <code style={{ backgroundColor: themeFontHoverColor }}>{children}</code>
  );
};

const useStyles = makeStyles({
  twitter: {
    color: 'inherit',
  },
});

export const AboutScene = function AboutScene({ onClickBack }) {
  const classes = useStyles();

  return (
    <div className={styles.centerMenuLayout}>
      <div className={styles.menuLayout}>
        <Typography variant="h1" gutterBottom>
          Snake!
        </Typography>
        <Typography variant="body1" gutterBottom>
          The snake arcade game in 2.5D! <br />
          Built with <Code>react</Code>, <Code>react-three-fiber</Code>, and{' '}
          <Code>threejs</Code>. Let me know what you think on{' '}
          <Link
            href="https://twitter.com/_natewilkins"
            rel="noreferrer"
            classes={{ root: classes.twitter }}
          >
            <TwitterIcon fontSize="inherit" />
            twitter
          </Link>
          !
        </Typography>
        <MenuItem isLink onClick={onClickBack}>
          Back
        </MenuItem>
      </div>
    </div>
  );
};
