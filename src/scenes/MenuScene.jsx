import React from 'react';
import { Typography } from '@material-ui/core';

import { MenuItem } from '../components/MenuItem';
import pkg from '../../package.json';

// eslint-disable-next-line
import styles from './Scene.css';

export const MenuScene = function MenuScene({
  onClickPlay,
  onClickSettings,
  onClickAbout,
}) {
  return (
    <>
      <div className={styles.centerMenuLayout}>
        <div className={styles.menuLayout}>
          <Typography variant="h1" gutterBottom>
            Snake!
          </Typography>
          <MenuItem isLink onClick={onClickPlay}>
            Play
          </MenuItem>
          <MenuItem isLink onClick={onClickSettings}>
            Settings
          </MenuItem>
          <MenuItem isLink onClick={onClickAbout}>
            About
          </MenuItem>
        </div>
      </div>
      <div style={{ position: 'absolute', right: 0, bottom: 0, margin: '1em' }}>
        <Typography variant="body1">v{pkg.version}</Typography>
      </div>
    </>
  );
};
