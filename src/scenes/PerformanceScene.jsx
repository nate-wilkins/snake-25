import React from 'react';
import {
  Typography,
  InputLabel,
  Input,
  Select,
  MenuItem as MuiMenuItem,
} from '@material-ui/core';

import { MenuItem } from '../components/MenuItem';
import { useSettingsProvider } from '../hooks/useSettingsProvider';

// eslint-disable-next-line
import styles from './Scene.css';

export const PerformanceScene = function PerformanceScene({ onClickBack }) {
  const settings = useSettingsProvider();
  const [performanceMode, setPerformanceMode] = settings.performanceMode;

  return (
    <div className={styles.centerMenuLayout}>
      <div className={styles.menuLayout}>
        <Typography variant="h1" gutterBottom>
          Snake!
        </Typography>

        <div style={{ display: 'flex', flexDirection: 'column' }}>
          <div className={styles.menuItemGroup}>
            <InputLabel id="performanceModeLabel">
              <MenuItem>Mode: </MenuItem>
            </InputLabel>
            <Select
              labelId="performanceModeLabel"
              id="performanceModeSelect"
              value={performanceMode}
              onChange={event => setPerformanceMode(event.target.value)}
              input={<Input />}
              label="Mode"
            >
              {[
                { value: false, name: 'Hide' },
                { value: 0, name: 'FPS' },
                { value: 1, name: 'Frame Time' },
                { value: 2, name: 'Memory' },
              ].map(({ name, value }) => (
                <MuiMenuItem key={value} value={value}>
                  <MenuItem>{name}</MenuItem>
                </MuiMenuItem>
              ))}
            </Select>
          </div>
        </div>

        <MenuItem isLink onClick={onClickBack}>
          Back
        </MenuItem>
      </div>
    </div>
  );
};
