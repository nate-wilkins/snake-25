import React, { useEffect, useRef, useState } from 'react';
import { ChromePicker } from 'react-color';
import { Typography } from '@material-ui/core';

import { MenuItem } from '../components/MenuItem';
import { useSettingsProvider } from '../hooks/useSettingsProvider';
import { useClickOutsideRef } from '../hooks/useClickOutsideRef';

// eslint-disable-next-line
import styles from './Scene.css';

const ColorSwatch = function ColorSwatch({ color, onChange }) {
  const dialogRef = useRef();
  const [toggle, setToggle] = useState(false);

  useClickOutsideRef(dialogRef, () => {
    setToggle(false);
  });

  return (
    <div>
      <button onClick={() => setToggle(toggle => !toggle)}>
        <div
          style={{ width: '60px', height: '15px', backgroundColor: color }}
        ></div>
      </button>

      {toggle && (
        <div
          ref={dialogRef}
          style={{ position: 'relative', marginLeft: '-135px' }}
        >
          <ChromePicker color={color} onChange={onChange} />
        </div>
      )}
    </div>
  );
};

export const ThemeScene = function ThemeScene({ onClickBack }) {
  const settings = useSettingsProvider();
  const [
    themeBackgroundColor,
    setThemeBackgroundColor,
  ] = settings.themeBackgroundColor;
  const [themeFontColor, setThemeFontColor] = settings.themeFontColor;
  const [
    themeFontHoverColor,
    setThemeFontHoverColor,
  ] = settings.themeFontHoverColor;
  const [themeMapColor, setThemeMapColor] = settings.themeMapColor;
  const [
    themeMapOutlineColor,
    setThemeOutlineColor,
  ] = settings.themeMapOutlineColor;
  const [themeSnakeColor, setThemeSnakeColor] = settings.themeSnakeColor;
  const [
    themeSnakeOutlineColor,
    setThemeSnakeOutlineColor,
  ] = settings.themeSnakeOutlineColor;
  const [themeAppleColor, setThemeAppleColor] = settings.themeAppleColor;
  const [
    themeAppleOutlineColor,
    setThemeAppleOutlineColor,
  ] = settings.themeAppleOutlineColor;
  const [
    themeGoldenAppleColor,
    setThemeGoldenAppleColor,
  ] = settings.themeGoldenAppleColor;
  const [
    themeGoldenAppleOutlineColor,
    setThemeGoldenAppleOutlineColor,
  ] = settings.themeGoldenAppleOutlineColor;
  const [
    themeMapEffectSplashColor,
    setThemeMapEffectSplashColor,
  ] = settings.themeMapEffectSplashColor;

  return (
    <div className={styles.centerMenuLayout}>
      <div className={styles.menuLayout}>
        <Typography variant="h1" gutterBottom>
          Snake!
        </Typography>

        <div style={{ display: 'flex', flexDirection: 'column' }}>
          <div className={styles.menuItemGroup}>
            <MenuItem small>Background:</MenuItem>
            <ColorSwatch
              color={themeBackgroundColor}
              onChange={color => setThemeBackgroundColor(color.hex)}
            />
          </div>
          <div className={styles.menuItemGroup}>
            <MenuItem small>Font: </MenuItem>
            <ColorSwatch
              color={themeFontColor}
              onChange={color => setThemeFontColor(color.hex)}
            />
          </div>
          <div className={styles.menuItemGroup}>
            <MenuItem small>Font Hover: </MenuItem>
            <ColorSwatch
              color={themeFontHoverColor}
              onChange={color => setThemeFontHoverColor(color.hex)}
            />
          </div>
          <div className={styles.menuItemGroup}>
            <MenuItem small>Map: </MenuItem>
            <ColorSwatch
              color={themeMapColor}
              onChange={color => setThemeMapColor(color.hex)}
            />
          </div>
          <div className={styles.menuItemGroup}>
            <MenuItem small>Map Outline: </MenuItem>
            <ColorSwatch
              color={themeMapOutlineColor}
              onChange={color => setThemeOutlineColor(color.hex)}
            />
          </div>
          <div className={styles.menuItemGroup}>
            <MenuItem small>Snake: </MenuItem>
            <ColorSwatch
              color={themeSnakeColor}
              onChange={color => setThemeSnakeColor(color.hex)}
            />
          </div>
          <div className={styles.menuItemGroup}>
            <MenuItem small>Snake Outline: </MenuItem>
            <ColorSwatch
              color={themeSnakeOutlineColor}
              onChange={color => setThemeSnakeOutlineColor(color.hex)}
            />
          </div>
          <div className={styles.menuItemGroup}>
            <MenuItem small>Apple: </MenuItem>
            <ColorSwatch
              color={themeAppleColor}
              onChange={color => setThemeAppleColor(color.hex)}
            />
          </div>
          <div className={styles.menuItemGroup}>
            <MenuItem small>Apple Outline: </MenuItem>
            <ColorSwatch
              color={themeAppleOutlineColor}
              onChange={color => setThemeAppleOutlineColor(color.hex)}
            />
          </div>
          <div className={styles.menuItemGroup}>
            <MenuItem small>Golden Apple: </MenuItem>
            <ColorSwatch
              color={themeGoldenAppleColor}
              onChange={color => setThemeGoldenAppleColor(color.hex)}
            />
          </div>
          <div className={styles.menuItemGroup}>
            <MenuItem small>Golden Apple Outline: </MenuItem>
            <ColorSwatch
              color={themeGoldenAppleOutlineColor}
              onChange={color => setThemeGoldenAppleOutlineColor(color.hex)}
            />
          </div>
          <div className={styles.menuItemGroup}>
            <MenuItem small>Splash Effect Color: </MenuItem>
            <ColorSwatch
              color={themeMapEffectSplashColor}
              onChange={color => setThemeMapEffectSplashColor(color.hex)}
            />
          </div>
        </div>

        <MenuItem isLink onClick={onClickBack}>
          Back
        </MenuItem>
      </div>
    </div>
  );
};

