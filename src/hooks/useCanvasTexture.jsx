import { useEffect } from 'react';
import { CanvasTexture } from 'three';

let canvasTextures = {};
const getCanvasTexture = (color, strokeColor, strokeWidth) => {
  return canvasTextures[`${color}-${strokeColor}-${strokeWidth}`];
};
const setCanvasTexture = (color, strokeColor, strokeWidth, texture) => {
  canvasTextures[`${color}-${strokeColor}-${strokeWidth}`] = texture;
  return texture;
};

export const useCanvasTexture = (color, strokeColor, strokeWidth) => {
  // Create canvas texture on mount.
  useEffect(() => {
    if (getCanvasTexture(color, strokeColor, strokeWidth)) {
      return;
    }
    const canvas = document.createElement('canvas');
    canvas.width = 16;
    canvas.height = 16;

    const ctx = canvas.getContext('2d');
    ctx.fillStyle = color;
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    ctx.strokeStyle = strokeColor;
    ctx.lineWidth = strokeWidth;
    ctx.strokeRect(0, 0, canvas.width, canvas.height);

    setCanvasTexture(
      color,
      strokeColor,
      strokeWidth,
      new CanvasTexture(canvas),
    );
  }, [color, strokeColor, strokeWidth]);

  return getCanvasTexture(color, strokeColor, strokeWidth);
};
