import { useEffect } from 'react';

export const useClickOutsideRef = (ref, callback) => {
  useEffect(() => {
    function handleOnClickDocument(event) {
      if (ref.current && !ref.current.contains(event.target)) {
        callback && callback();
      }
    }

    document.addEventListener('mousedown', handleOnClickDocument);
    return () => {
      document.removeEventListener('mousedown', handleOnClickDocument);
    };
  }, [ref]);
};
