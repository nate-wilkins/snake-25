import { useContext } from 'react';

import { GameProviderContext } from './../providers/GameProvider';

export const useGameProvider = function useGameProvider() {
  const context = useContext(GameProviderContext);
  if (!context) {
    throw new Error("'useGameProvider' must be used within a 'GameProvider'.");
  }
  return context;
};
