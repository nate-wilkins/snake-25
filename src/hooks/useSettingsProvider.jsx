import { useContext } from 'react';

import { SettingsProviderContext } from './../providers/SettingsProvider';

export const useSettingsProvider = function useSettingsProvider() {
  const context = useContext(SettingsProviderContext);
  if (!context) {
    throw new Error(
      "'useSettingsProvider' must be used within a 'SettingsProvider'.",
    );
  }
  return context;
};
