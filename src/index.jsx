import React from 'react';
import ReactDOM from 'react-dom';
// eslint-disable-next-line
import styles from './index.global.css';

import { App } from './App';

document.addEventListener('DOMContentLoaded', () => {
  const rootElement = document.getElementById('root');
  ReactDOM.render(<App />, rootElement);
});

module.hot.accept();
