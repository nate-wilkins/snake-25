import React, { useEffect, useState } from 'react';
import { CssBaseline } from '@material-ui/core';
import { ThemeProvider } from '@material-ui/styles';

import { createMuiTheme } from './muiSettings';
import { SettingsProvider } from './providers/SettingsProvider';
import { SceneBackdrop } from './scenes/SceneBackdrop';
import { AboutScene } from './scenes/AboutScene';
import { MenuScene } from './scenes/MenuScene';
import { SettingsScene } from './scenes/SettingsScene';
import { ThemeScene } from './scenes/ThemeScene';
import { GameScene } from './scenes/GameScene';
import { PerformanceScene } from './scenes/PerformanceScene';
import { useSettingsProvider } from './hooks/useSettingsProvider';

export const Theme = function Theme({ children }) {
  const settings = useSettingsProvider();
  const muiTheme = createMuiTheme({
    backgroundColor: settings.themeBackgroundColor[0],
    fontColor: settings.themeFontColor[0],
    fontHoverColor: settings.themeFontHoverColor[0],
  });
  const [render, setRender] = useState(false);

  // NOTE: Attempt to remove text changes due to theme not applying fast enough.
  useEffect(() => {
    const timer = setTimeout(() => {
      setRender(true);
    }, 800);
    return () => {
      clearTimeout(timer);
    };
  });

  return (
    <>
      <ThemeProvider theme={muiTheme}>
        <CssBaseline />
        {render && children}
      </ThemeProvider>
    </>
  );
};

export const App = function App() {
  const [scene, setScene] = useState('MENU');

  let sceneComponent;
  if (scene === 'MENU') {
    sceneComponent = (
      <MenuScene
        onClickPlay={() => setScene('GAME')}
        onClickSettings={() => setScene('SETTINGS')}
        onClickAbout={() => setScene('ABOUT')}
      />
    );
  } else if (scene === 'ABOUT') {
    sceneComponent = <AboutScene onClickBack={() => setScene('MENU')} />;
  } else if (scene === 'SETTINGS') {
    sceneComponent = (
      <SettingsScene
        onClickBack={() => setScene('MENU')}
        onClickTheme={() => setScene('THEME')}
        onClickPerformance={() => setScene('PERFORMANCE')}
      />
    );
  } else if (scene === 'THEME') {
    sceneComponent = <ThemeScene onClickBack={() => setScene('SETTINGS')} />;
  } else if (scene === 'PERFORMANCE') {
    sceneComponent = (
      <PerformanceScene onClickBack={() => setScene('SETTINGS')} />
    );
  } else if (scene === 'GAME') {
    sceneComponent = <GameScene onReplay={() => setScene('MENU')} />;
  } else {
    throw new Error(`No scene '${scene}'.`);
  }

  return (
    <SettingsProvider>
      <Theme>
        <SceneBackdrop>{sceneComponent}</SceneBackdrop>
      </Theme>
    </SettingsProvider>
  );
};
