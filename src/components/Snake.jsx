import React from 'react';
import { useFrame } from 'react-three-fiber';

import { useSettingsProvider } from '../hooks/useSettingsProvider';
import { Block } from './Block';
import { getPaddedPosition } from './Map';

export const Snake = function Snake({ pieces, wireframe }) {
  const settings = useSettingsProvider();
  const [themeSnakeColor, _setThemeSnakeColor] = settings.themeSnakeColor;
  const [
    themeSnakeOutlineColor,
    _setThemeSnakeOutlineColor,
  ] = settings.themeSnakeOutlineColor;

  // Set camera to follow head of snake.
  useFrame(state => {
    const snakeHeadX = pieces[0].d_x;
    const snakeHeadZ = pieces[0].d_z;

    state.camera.position.set(
      getPaddedPosition(snakeHeadX - 14),
      getPaddedPosition(19),
      getPaddedPosition(snakeHeadZ),
    );
    state.camera.lookAt(
      getPaddedPosition(snakeHeadX),
      getPaddedPosition(1),
      getPaddedPosition(snakeHeadZ),
    );
    state.camera.updateProjectionMatrix();
  });

  return (
    <>
      {pieces.map(({ id, d_x, d_y, d_z }, i) =>
        i === 0 ? (
          <Block
            key={id}
            wireframe={wireframe}
            color={themeSnakeColor}
            strokeColor={themeSnakeOutlineColor}
            strokeWidth={2}
            scale={[0.9, 0.9, 0.9]}
            position={[
              getPaddedPosition(d_x),
              getPaddedPosition(d_y),
              getPaddedPosition(d_z),
            ]}
          />
        ) : (
          <Block
            key={id}
            wireframe={wireframe}
            color={themeSnakeColor}
            strokeColor={themeSnakeOutlineColor}
            strokeWidth={2}
            scale={[0.9, 0.9, 0.9]}
            position={[
              getPaddedPosition(d_x),
              getPaddedPosition(d_y),
              getPaddedPosition(d_z),
            ]}
          />
        ),
      )}
    </>
  );
};

