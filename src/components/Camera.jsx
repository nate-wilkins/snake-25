import React, { useEffect, useRef } from 'react';
import { useFrame, useThree } from 'react-three-fiber';

export const Camera = function Camera(props) {
  const ref = useRef();
  const { size, setDefaultCamera } = useThree();
  // Make the camera known to threejs.
  useEffect(() => void setDefaultCamera(ref.current), []);
  useFrame(() => ref.current.updateMatrixWorld());
  return (
    <perspectiveCamera ref={ref} {...props} aspect={size.width / size.height} />
  );
};
