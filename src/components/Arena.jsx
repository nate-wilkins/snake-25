import React, { useEffect, useRef, useState } from 'react';
import { useFrame } from 'react-three-fiber';
import lerp from 'lerp';
import { v4 as uuidv4 } from 'uuid';

import { useGameProvider } from './../hooks/useGameProvider';
import { Camera } from './Camera';
import { Snake } from './Snake';
import { Map, getPaddedPosition } from './Map';
import { Apple } from './Apple';

const SPEED_INTERVAL_MIN = 0.1;
const SPEED_INTERVAL_START = 0.5;

const getRandomInt = function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min) + min); //The maximum is exclusive and the minimum is inclusive
};

export const Arena = function Arena({ width, height }) {
  const game = useGameProvider();
  const [_score, setScore] = game.score;
  const [_scoreIncrements, setScoreIncrements] = game.scoreIncrements;
  const [gamePaused, _setGamePaused] = game.gamePaused;
  const [gameOver, setGameOver] = game.gameOver;
  const [lastGoodSnakeDirection, setLastGoodSnakeDirection] = useState('U');
  const [snakeDirection, setSnakeDirection] = game.snakeDirection;
  const [wireframe, _setWireframe] = game.wireframe;
  const [showAxisHelper, _showAxisHelper] = game.showAxisHelper;

  // Map effects.
  const [mapEffects, setMapEffects] = useState([]);

  // Create snake.
  const [speedInterval, setSpeedInterval] = useState(SPEED_INTERVAL_START);
  // Amount of apples needed to be eaten before reaching min speed interval is 1/3 the total map area.
  // SPEED_INTERVAL_START - ((WIDTH * HEIGHT / 3) * SPEED_INTERVAL_DECREMENT) = SPEED_INTERVAL_MIN
  const speedIntervalDecay =
    (-1 * SPEED_INTERVAL_MIN + SPEED_INTERVAL_START) / ((width * height) / 3);
  const snakeElapsedTime = useRef(0);
  const snakeHeadX = Math.floor(width / 2);
  const snakeHeadZ = Math.floor(height / 2);
  const [snakePieces, setSnakePieces] = useState([
    {
      id: uuidv4(),
      x: snakeHeadX,
      y: 1,
      z: snakeHeadZ,
      d_x: snakeHeadX,
      d_y: 1,
      d_z: snakeHeadZ,
    },
    {
      id: uuidv4(),
      x: snakeHeadX - 1,
      y: 1,
      z: snakeHeadZ,
      d_x: snakeHeadX - 1,
      d_y: 1,
      d_z: snakeHeadZ,
    },
    {
      id: uuidv4(),
      x: snakeHeadX - 2,
      y: 1,
      z: snakeHeadZ,
      d_x: snakeHeadX - 2,
      d_y: 1,
      d_z: snakeHeadZ,
    },
  ]);

  // Create apple.
  const getRandomApple = () => {
    const availablePositions = [];
    for (let z = 1; z < height - 1; z++) {
      for (let x = 1; x < width - 1; x++) {
        if (!snakePieces.some(piece => piece.x === x && piece.z === z)) {
          availablePositions.push({
            x,
            z,
            y: 1,
          });
        }
      }
    }
    return {
      ...availablePositions[getRandomInt(0, availablePositions.length)],
      isGolden: getRandomInt(0, 16) === 1, // (1/15)th chance to spawn.
    };
  };
  const [apple, setApple] = useState(null);
  useEffect(() => {
    setApple(getRandomApple());
  }, []);

  const calculateNextHeadPosition = (head, direction) => {
    let new_z = head.z;
    let new_x = head.x;
    let new_y = head.y;

    if (direction === 'R') {
      new_z += 1;
    } else if (direction === 'U') {
      new_x += 1;
    } else if (direction === 'L') {
      new_z -= 1;
    } else if (direction === 'D') {
      new_x -= 1;
    }

    return {
      x: new_x,
      y: 1,
      z: new_z,
    };
  };

  const updateSnakePiecesFromMovement = ({ delta }) => {
    // Update pieces forward based on game speed.
    snakeElapsedTime.current += delta;
    if (snakeElapsedTime.current <= speedInterval) {
      return;
    }
    snakeElapsedTime.current = 0;

    // Calculate new head.
    const head = snakePieces[0];
    const firstBodyPiece = snakePieces[1];
    let nextHeadPos = calculateNextHeadPosition(head, snakeDirection);
    // Validate next position is valid - if not continue with last valid move.
    if (
      nextHeadPos.z === firstBodyPiece.z &&
      nextHeadPos.x === firstBodyPiece.x
    ) {
      nextHeadPos = calculateNextHeadPosition(head, lastGoodSnakeDirection);
    } else {
      setLastGoodSnakeDirection(snakeDirection);
    }

    // Update pieces.
    setSnakePieces(snakePieces => {
      const newSnakePieces = [];
      for (let i = 1; i <= snakePieces.length; i++) {
        const pieceIndexToUpdate = snakePieces.length - i;
        const pieceToUpdate = snakePieces[pieceIndexToUpdate];

        if (i !== snakePieces.length) {
          // Update body piece.
          const pieceAhead = snakePieces[snakePieces.length - i - 1];
          newSnakePieces[pieceIndexToUpdate] = {
            id: pieceToUpdate.id,
            x: pieceAhead.x,
            y: pieceAhead.y,
            z: pieceAhead.z,
            d_x: pieceToUpdate.d_x,
            d_y: pieceToUpdate.d_y,
            d_z: pieceToUpdate.d_z,
          };
        } else {
          // Update head piece.
          newSnakePieces[pieceIndexToUpdate] = {
            id: pieceToUpdate.id,
            x: nextHeadPos.x,
            y: nextHeadPos.y,
            z: nextHeadPos.z,
            d_x: pieceToUpdate.d_x,
            d_y: pieceToUpdate.d_y,
            d_z: pieceToUpdate.d_z,
          };
        }
      }
      return newSnakePieces;
    });
  };

  const updateSnakePiecesAnimation = ({ delta }) => {
    // Update pieces toward their desired position, animating with linear interpolation.
    setSnakePieces(snakePieces => {
      const newSnakePieces = [];
      for (let i = 0; i < snakePieces.length; i++) {
        const piece = snakePieces[i];
        newSnakePieces[i] = {
          id: piece.id,
          x: piece.x,
          y: piece.y,
          z: piece.z,
          d_x: lerp(piece.d_x, piece.x, (delta / speedInterval) * 2),
          d_y: lerp(piece.d_y, piece.y, (delta / speedInterval) * 2),
          d_z: lerp(piece.d_z, piece.z, (delta / speedInterval) * 2),
        };
      }
      return newSnakePieces;
    });
  };

  const updateApple = () => {
    // Checks whether an apple was eaten or not.
    // Grows the snake if so and increases game speed.
    const head = snakePieces[0];

    if (head.x === apple.x && head.y === apple.y && head.z === apple.z) {
      const increment = Math.floor(
        (0.1 / speedInterval) * (apple.isGolden ? 20 : 15),
      );
      setMapEffects(mapEffects => [
        ...mapEffects,
        {
          x: apple.x,
          y: apple.y,
          z: apple.z,
          type: 'SPLASH',
        },
      ]);
      setApple(getRandomApple());
      setScoreIncrements(scoreIncrements => [
        ...scoreIncrements,
        {
          id: uuidv4(),
          value: increment,
        },
      ]);
      setScore(score => {
        return score + increment;
      });
      setSnakePieces(snakePieces => {
        const lastBodyPiece = snakePieces[snakePieces.length - 1];
        return [
          ...snakePieces,
          {
            id: uuidv4(),
            x: lastBodyPiece.x,
            y: lastBodyPiece.y,
            z: lastBodyPiece.z,
            d_x: lastBodyPiece.d_x,
            d_y: lastBodyPiece.d_y,
            d_z: lastBodyPiece.d_z,
          },
        ];
      });
      setSpeedInterval(speedInterval => {
        const newSpeedInterval = speedInterval - speedIntervalDecay;
        if (newSpeedInterval <= SPEED_INTERVAL_MIN) {
          return SPEED_INTERVAL_MIN;
        } else {
          return newSpeedInterval;
        }
      });
    }
  };

  const updateCollision = () => {
    // Checks whether the snake head has collided with the map walls or itself.
    const head = snakePieces[0];

    // Check head against body.
    for (let i = 1; i < snakePieces.length; i++) {
      const piece = snakePieces[i];
      if (head.x === piece.x && head.y === piece.y && head.z === piece.z) {
        setGameOver(true);
        break;
      }
    }

    // Check head against walls.
    if (
      head.z <= 0 ||
      head.x <= 0 ||
      head.z >= height - 1 ||
      head.x >= width - 1
    ) {
      setGameOver(true);
    }
  };

  // Game loop.
  useFrame((_state, delta) => {
    if (gamePaused || gameOver) {
      return;
    }
    updateSnakePiecesFromMovement({ delta });
    updateSnakePiecesAnimation({ delta });
    updateApple();
    updateCollision();
  });

  const pointLightPosX = Math.floor(width / 2);
  const pointLightPosZ = Math.floor(height / 2);
  const pointLightPosY = 10;

  return (
    <>
      <Camera fov={45} near={0.005} far={10000} />
      <ambientLight intensity={0.5} distance={50} color="white" />
      {showAxisHelper && (
        <axesHelper
          size={6}
          position={[pointLightPosX, pointLightPosY, pointLightPosZ]}
        />
      )}
      <pointLight
        intensity={0.5}
        position={[pointLightPosX, pointLightPosY, pointLightPosZ]}
      />
      <Map
        width={width}
        height={height}
        wireframe={wireframe}
        effects={mapEffects}
        onEffectsApplied={remainingMapEffects =>
          setMapEffects(remainingMapEffects)
        }
      />
      <Snake pieces={snakePieces} wireframe={wireframe} />
      {apple && (
        <Apple
          position={[
            getPaddedPosition(apple.x),
            getPaddedPosition(apple.y),
            getPaddedPosition(apple.z),
          ]}
          isGolden={apple.isGolden}
          wireframe={wireframe}
        />
      )}
    </>
  );
};

