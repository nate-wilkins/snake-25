import React from 'react';

import { useSettingsProvider } from '../hooks/useSettingsProvider';
import { Block } from './Block';

export const Apple = function Apple({ position, isGolden, wireframe }) {
  const settings = useSettingsProvider();
  const [themeAppleColor, _setThemeAppleColor] = settings.themeAppleColor;
  const [
    themeAppleOutlineColor,
    _setThemeAppleOutlineColor,
  ] = settings.themeAppleOutlineColor;
  const [
    themeGoldenAppleColor,
    _setThemeGoldenAppleColor,
  ] = settings.themeGoldenAppleColor;
  const [
    themeGoldenAppleOutlineColor,
    _setThemeGoldenAppleOutlineColor,
  ] = settings.themeGoldenAppleOutlineColor;

  return !isGolden ? (
    <Block
      wireframe={wireframe}
      color={themeAppleColor}
      strokeColor={themeAppleOutlineColor}
      strokeWidth={2}
      scale={[1, 1, 1]}
      position={position}
    />
  ) : (
    <Block
      wireframe={wireframe}
      color={themeGoldenAppleColor}
      strokeColor={themeGoldenAppleOutlineColor}
      strokeWidth={2}
      scale={[1, 1, 1]}
      position={position}
    />
  );
};
