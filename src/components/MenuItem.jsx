import React, { useRef, useState } from 'react';
import { Typography } from '@material-ui/core';

import { useSettingsProvider } from '../hooks/useSettingsProvider';

export const MenuItem = function MenuItem({
  onClick,
  isLink,
  small,
  children,
}) {
  const settings = useSettingsProvider();
  const [
    themeFontHoverColor,
    _setThemeFontHoverColor,
  ] = settings.themeFontHoverColor;

  const ref = useRef();
  const [isMouseHovering, setIsMouseHovering] = useState(false);

  return (
    <div
      ref={ref}
      style={{
        marginBottom: '0.8em',
        color: isLink && isMouseHovering ? themeFontHoverColor : 'inherit',
        ...(isLink
          ? {
              cursor: 'pointer',
            }
          : {}),
      }}
      onClick={onClick}
      onMouseOver={() => setIsMouseHovering(true)}
      onMouseLeave={() => setIsMouseHovering(false)}
    >
      {!small ? (
        <Typography variant="h4" style={{ margin: 0 }}>
          {children}
        </Typography>
      ) : (
        <Typography variant="h6" style={{ margin: 0 }}>
          {children}
        </Typography>
      )}
    </div>
  );
};
