import React, { useContext, createElement } from 'react';
import { Canvas as ThreeCanvas } from 'react-three-fiber';
import { OrbitControls, Stats } from 'drei';

import { GameProviderContext } from '../providers/GameProvider';
import { SettingsProviderContext } from '../providers/SettingsProvider';
import { useSettingsProvider } from '../hooks/useSettingsProvider';

// NOTE: Bug in react context.
// https://github.com/pmndrs/react-three-fiber/issues/43#issuecomment-694849918
const forwardContext =
  Context =>
  Component =>
  ({ children, ...props }) => {
    const value = useContext(Context);
    const wrapped = createElement(Context.Provider, { value }, children);
    return createElement(Component, props, wrapped);
  };

const ThreeCanvasWithContext = forwardContext(SettingsProviderContext)(
  forwardContext(GameProviderContext)(ThreeCanvas),
);

export const Canvas = function Canvas({
  style,
  controls,
  clearColor,
  children,
}) {
  const settings = useSettingsProvider();
  const [performanceMode, _setPerformanceMode] = settings.performanceMode;

  return (
    <ThreeCanvasWithContext
      style={style}
      gl={{ antialias: false, alpha: false }}
      colorManagement
      shadowMap
      tabindex={0}
      resize={{ scroll: false }}
      pixelRatio={window.devicePixelRatio}
      onCreated={({ gl }) => gl.setClearColor(clearColor)}
    >
      {children}
      {performanceMode !== false && <Stats showPanel={performanceMode} />}
      {controls && (
        <OrbitControls enablePan enableRotate enableZoom zoomSpeed={0.5} />
      )}
    </ThreeCanvasWithContext>
  );
};
