import React, { useEffect, useMemo, useRef, useState } from 'react';
import { useFrame } from 'react-three-fiber';

import { useSettingsProvider } from '../hooks/useSettingsProvider';

import { Block } from './Block';

const BLOCK_PADDING = 0.2;

export const getPaddedPosition = x => {
  return x * BLOCK_PADDING + x;
};

export const Map = function Map({
  width,
  height,
  effects,
  onEffectsApplied,
  wireframe,
}) {
  const settings = useSettingsProvider();
  const [themeMapColor, _setThemeMapColor] = settings.themeMapColor;
  const [
    themeMapEffectSplashColor,
    _setThemeMapEffectSplashColor,
  ] = settings.themeMapEffectSplashColor;
  const effectSplashTimeInterval = 0.1;
  const [
    themeMapOutlineColor,
    _setThemeMapOutlineColor,
  ] = settings.themeMapOutlineColor;

  // Map blocks.
  const [blocks, setBlocks] = useState([]);
  useEffect(() => {
    const mapBlocks = [];

    // Create floor.
    for (let x = 0; x < width; x++) {
      for (let z = 0; z < height; z++) {
        mapBlocks.push({
          x: x,
          y: 0,
          z: z,
          color: themeMapColor,
        });
      }
    }

    // Create walls - while removing duplicate corners.
    for (let top = 0; top < width; top++) {
      mapBlocks.push({
        x: top,
        y: 1,
        z: 0,
        color: themeMapColor,
      });
    }
    for (let bottom = 0; bottom < width; bottom++) {
      mapBlocks.push({
        x: bottom,
        y: 1,
        z: height - 1,
        color: themeMapColor,
      });
    }
    for (let left = 1; left < height - 1; left++) {
      mapBlocks.push({
        x: 0,
        y: 1,
        z: left,
        color: themeMapColor,
      });
    }
    for (let right = 1; right < height - 1; right++) {
      mapBlocks.push({
        x: width - 1,
        y: 1,
        z: right,
        color: themeMapColor,
      });
    }

    setBlocks(mapBlocks);
  }, []);

  // Splash effects.
  // Use a reference here to avoid state effect rerenders.
  // Effects also only apply to changing the map blocks.
  const effectSplashes = useRef([]);
  useEffect(() => {
    if (effects.length <= 0) return;

    for (let effect of effects) {
      if (effect.type === 'SPLASH') {
        effectSplashes.current.push({
          x_inc: 1,
          y_inc: 0,
          z_inc: 0,
          x: effect.x + 1,
          y: effect.y,
          z: effect.z,
          length: 1,
          color: themeMapEffectSplashColor,
          original_x: effect.x + 1,
          original_y: effect.y,
          original_z: effect.z,
        });
        effectSplashes.current.push({
          x_inc: -1,
          y_inc: 0,
          z_inc: 0,
          x: effect.x - 1,
          y: effect.y,
          z: effect.z,
          length: 1,
          color: themeMapEffectSplashColor,
          original_x: effect.x - 1,
          original_y: effect.y,
          original_z: effect.z,
        });
        effectSplashes.current.push({
          x_inc: 0,
          y_inc: 0,
          z_inc: -1,
          x: effect.x,
          y: effect.y,
          z: effect.z - 1,
          length: 1,
          color: themeMapEffectSplashColor,
          original_x: effect.x,
          original_y: effect.y,
          original_z: effect.z - 1,
        });
        effectSplashes.current.push({
          x_inc: 0,
          y_inc: 0,
          z_inc: 1,
          x: effect.x,
          y: effect.y,
          z: effect.z + 1,
          length: 1,
          color: themeMapEffectSplashColor,
          original_x: effect.x,
          original_y: effect.y,
          original_z: effect.z + 1,
        });
      }
    }

    onEffectsApplied([]);
  }, [effects]);

  const effectSplashElapsedTime = useRef(0);
  useFrame((_state, delta) => {
    // Update pieces forward based on game speed.
    effectSplashElapsedTime.current += delta;
    if (effectSplashElapsedTime.current <= effectSplashTimeInterval) {
      return;
    }
    effectSplashElapsedTime.current = 0;

    // Iterate through effects.
    // Find map piece corresponding to effect
    let newBlocks = [...blocks];
    for (let effectSplash of effectSplashes.current) {
      let newBlockIndex = 0;
      for (let block of blocks) {
        // Apply to blocks vertically.
        if (effectSplash.x === block.x && effectSplash.z === block.z) {
          // Apply to all blocks at effect position.
          newBlocks[newBlockIndex].color = effectSplash.color;
        } else if (
          effectSplash.x + effectSplash.z_inc * effectSplash.length >=
            block.x &&
          effectSplash.x + effectSplash.z_inc * -1 * effectSplash.length <=
            block.x &&
          effectSplash.z === block.z
        ) {
          // Apply to all blocks in "right" direction given length of effect.
          newBlocks[newBlockIndex].color = effectSplash.color;
        } else if (
          effectSplash.x - effectSplash.z_inc * effectSplash.length >=
            block.x &&
          effectSplash.x - effectSplash.z_inc * -1 * effectSplash.length <=
            block.x &&
          effectSplash.z === block.z
        ) {
          // Apply to all blocks in "left" direction given length of effect.
          newBlocks[newBlockIndex].color = effectSplash.color;
        } else if (
          effectSplash.z - effectSplash.x_inc * effectSplash.length >=
            block.z &&
          effectSplash.z - effectSplash.x_inc * -1 * effectSplash.length <=
            block.z &&
          effectSplash.x === block.x
        ) {
          // Apply to all blocks in "down" direction given length of effect.
          newBlocks[newBlockIndex].color = effectSplash.color;
        } else if (
          effectSplash.z + effectSplash.x_inc * effectSplash.length >=
            block.z &&
          effectSplash.z + effectSplash.x_inc * -1 * effectSplash.length <=
            block.z &&
          effectSplash.x === block.x
        ) {
          // Apply to all blocks in "up" direction given length of effect.
          newBlocks[newBlockIndex].color = effectSplash.color;
        }
        newBlockIndex++;
      }

      // Clear splash effect color.
      if (
        effectSplash.color === themeMapEffectSplashColor &&
        effectSplash.length === 3
      ) {
        effectSplashes.current.push({
          x_inc: effectSplash.x_inc,
          y_inc: effectSplash.y_inc,
          z_inc: effectSplash.z_inc,
          x: effectSplash.original_x,
          y: effectSplash.original_y,
          z: effectSplash.original_z,
          length: 1,
          color: themeMapColor,
          original_x: effectSplash.original_x,
          original_y: effectSplash.original_y,
          original_z: effectSplash.original_z,
        });
      }

      if (
        effectSplash.x <= 0 ||
        effectSplash.z <= 0 ||
        effectSplash.x >= width ||
        effectSplash.z >= height
      ) {
        // Remove splash effect when it reaches the edges of the map.
        effectSplash.deleted = true;
        // Check to see if the effect created a cleanup effect.
        if (
          effectSplash.color === themeMapEffectSplashColor &&
          !effectSplashes.current.find(
            s =>
              s.color === themeMapColor &&
              s.original_x === effectSplash.original_x &&
              s.original_y === effectSplash.original_y &&
              s.original_z === effectSplash.original_z,
          )
        ) {
          effectSplashes.current.push({
            x_inc: effectSplash.x_inc,
            y_inc: effectSplash.y_inc,
            z_inc: effectSplash.z_inc,
            x: effectSplash.original_x,
            y: effectSplash.original_y,
            z: effectSplash.original_z,
            length: 1,
            color: themeMapColor,
            original_x: effectSplash.original_x,
            original_y: effectSplash.original_y,
            original_z: effectSplash.original_z,
          });
        }
      } else {
        // Apply effect.
        effectSplash.x += effectSplash.x_inc;
        effectSplash.y += effectSplash.y_inc;
        effectSplash.z += effectSplash.z_inc;
        effectSplash.length += 1;
      }
    }
    setBlocks(newBlocks);
    effectSplashes.current = effectSplashes.current.filter(e => !e.deleted);
  });

  return (
    <>
      {blocks.map(({ x, y, z, color }) => (
        <Block
          key={`${x}-${y}-${z}-${color}`}
          wireframe={wireframe}
          color={color}
          strokeColor={themeMapOutlineColor}
          strokeWidth={2}
          scale={[1, 1, 1]}
          position={[
            getPaddedPosition(x),
            getPaddedPosition(y),
            getPaddedPosition(z),
          ]}
        />
      ))}
    </>
  );
};

