import React, { useRef } from 'react';

import { useCanvasTexture } from '../hooks/useCanvasTexture';

export const Block = function Block({
  position,
  color,
  strokeColor,
  strokeWidth,
  scale,
  wireframe,
}) {
  const group = useRef();
  const mesh = useRef();

  const canvasTexture = useCanvasTexture(color, strokeColor, strokeWidth);

  return (
    <group ref={group} dispose={null}>
      <group rotation={[Math.PI / 1, 0, 0]} position={position}>
        <mesh ref={mesh} scale={scale}>
          <boxBufferGeometry args={[1, 1, 1]} />
          <meshStandardMaterial wireframe={wireframe} map={canvasTexture} />
        </mesh>
      </group>
    </group>
  );
};
