import React, { useEffect, useState } from 'react';
import { useDrag } from 'react-use-gesture';
import { Typography } from '@material-ui/core';

import { useGameProvider } from './../hooks/useGameProvider';
import { MenuItem } from './MenuItem';
// eslint-disable-next-line
import styles from './GameHud.css';

const KEYCODE_ESC = 27;
const KEYCODE_UP = 38;
const KEYCODE_W = 87;
const KEYCODE_RIGHT = 39;
const KEYCODE_D = 68;
const KEYCODE_LEFT = 37;
const KEYCODE_A = 65;
const KEYCODE_DOWN = 40;
const KEYCODE_S = 83;
const KEYCODE_ENTER = 13;

// Should align with css animation.
const SCORE_INCREMENT_INDICATOR_TIMEOUT = 2000;

export const GameHud = function GameHud({ onReplay }) {
  const game = useGameProvider();
  const [score, _setScore] = game.score;
  const [scoreIncrements, setScoreIncrements] = game.scoreIncrements;
  const [gameOver, _setGameOver] = game.gameOver;
  const [gamePaused, setGamePaused] = game.gamePaused;
  const [snakeDirection, setSnakeDirection] = game.snakeDirection;

  // Snake controls.
  const handleKeydown = event => {
    const { keyCode } = event;

    // Up, down, right, left.
    let newSnakeDirection = snakeDirection;
    if (keyCode === KEYCODE_UP || keyCode === KEYCODE_W) {
      newSnakeDirection = 'U';
    } else if (keyCode === KEYCODE_DOWN || keyCode === KEYCODE_S) {
      newSnakeDirection = 'D';
    } else if (keyCode === KEYCODE_RIGHT || keyCode === KEYCODE_D) {
      newSnakeDirection = 'R';
    } else if (keyCode === KEYCODE_LEFT || keyCode === KEYCODE_A) {
      newSnakeDirection = 'L';
    }
    setSnakeDirection(newSnakeDirection);

    // Paused game?
    if (keyCode === KEYCODE_ESC) {
      setGamePaused(gamePaused => !gamePaused);
    }

    // Keyboard shortcuts for game over.
    if (gameOver) {
      if (keyCode === KEYCODE_ENTER) {
        onReplay && onReplay();
      }
    }
  };
  useEffect(() => {
    document.addEventListener('keydown', handleKeydown);
    return () => {
      document.removeEventListener('keydown', handleKeydown);
    };
  }, [handleKeydown]);

  const handleDragGesture = ({ down, movement: [mx, my] }) => {
    // Skip if we aren't dragging yet.
    if (!down) return;

    // Drag angle.
    const angle = (Math.atan2(my, mx) * 180) / Math.PI;
    let newSnakeDirection = snakeDirection;
    if (angle < -45 && angle > -135) {
      newSnakeDirection = 'U';
    } else if (angle >= -45 && angle <= 45) {
      newSnakeDirection = 'R';
    } else if (angle > 45 && angle < 135) {
      newSnakeDirection = 'D';
    } else if (
      (angle >= 135 && angle <= 180) ||
      (angle >= -135 && angle <= -180)
    ) {
      newSnakeDirection = 'L';
    }
    setSnakeDirection(newSnakeDirection);
  };
  const bind = useDrag(handleDragGesture, {
    // Skip if we haven't traveled a min threshold.
    // Can cause a bug where user clicks with angle of 0.
    threshold: 5,
  });

  // Score increment indicator.
  const [scoreIncrementTimers, setScoreIncrementTimers] = useState([]);
  useEffect(() => {
    if (scoreIncrements.length <= 0) return;
    if (scoreIncrements.length <= scoreIncrementTimers.length) return;
    // Make every increment have its own timer decay.
    for (let i = scoreIncrementTimers.length; i < scoreIncrements.length; i++) {
      const timer = setTimeout(() => {
        setScoreIncrements(scoreIncrements => {
          return [...scoreIncrements].slice(1);
        });
        setScoreIncrementTimers(scoreIncrementTimers => {
          return [...scoreIncrementTimers].slice(1);
        });
      }, SCORE_INCREMENT_INDICATOR_TIMEOUT);
      setScoreIncrementTimers(scoreIncrementTimers => {
        return [...scoreIncrementTimers, timer];
      });
    }
  }, [scoreIncrements]);

  return (
    <div
      {...bind()}
      style={{
        touchAction: 'none',
      }}
    >
      <div className={styles.score}>
        <Typography variant="h5">Score: {score}</Typography>
        {scoreIncrements.map((increment, i) => (
          <Typography
            variant="h6"
            key={increment.id}
            className={styles.scoreIncrement}
          >
            +{increment.value}
          </Typography>
        ))}
      </div>
      <div className={styles.overlay}>
        <div className={styles.overlayItems}>
          <Typography variant="h2" gutterBottom>
            {gameOver ? 'Game Over' : gamePaused ? 'PAUSED' : ''}
          </Typography>
          {gameOver && (
            <MenuItem isLink onClick={onReplay}>
              Play Again?
            </MenuItem>
          )}
        </div>
      </div>
    </div>
  );
};
