import React, { createContext, useState } from 'react';

export const GameProviderContext = createContext(null);

export const GameProvider = function GameProvider({ children }) {
  const [wireframe, setWireframe] = useState(false);
  const [showAxisHelper, setShowAxisHelper] = useState(false);
  const [score, setScore] = useState(0);
  const [scoreIncrements, setScoreIncrements] = useState([]);
  const [gameOver, setGameOver] = useState(false);
  const [gamePaused, setGamePaused] = useState(false);
  const [snakeDirection, setSnakeDirection] = useState('U');

  return (
    <GameProviderContext.Provider
      value={{
        wireframe: [wireframe, setWireframe],
        score: [score, setScore],
        scoreIncrements: [scoreIncrements, setScoreIncrements],
        gameOver: [gameOver, setGameOver],
        gamePaused: [gamePaused, setGamePaused],
        snakeDirection: [snakeDirection, setSnakeDirection],
        showAxisHelper: [showAxisHelper, setShowAxisHelper],
      }}
    >
      {children}
    </GameProviderContext.Provider>
  );
};
