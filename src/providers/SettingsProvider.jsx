import React, { createContext, useState } from 'react';

import { useLocalStorage } from './../hooks/useLocalStorage';

export const SettingsProviderContext = createContext(null);

const ARENA_DEFAULT_WIDTH = 12;
const ARENA_MAXIMUM_WIDTH = 18;
const ARENA_DEFAULT_HEIGHT = 12;
const ARENA_MAXIMUM_HEIGHT = 18;

export const SettingsProvider = function SettingsProvider({ children }) {
  // Theme settings.
  const [theme, setTheme] = useLocalStorage('theme', 'CYBER_PUNK');
  const [themeBackgroundColor, setThemeBackgroundColor] = useLocalStorage(
    'themeBackgroundColor',
    '#121212',
  );
  const [themeFontColor, setThemeFontColor] = useLocalStorage(
    'themeFontColor',
    '#F9F9F9',
  );
  const [themeFontHoverColor, setThemeFontHoverColor] = useLocalStorage(
    'themeFontHoverColor',
    '#7A7A7A',
  );
  const [themeMapColor, setThemeMapColor] = useLocalStorage(
    'themeMapColor',
    '#111111',
  );
  const [themeMapOutlineColor, setThemeOutlineColor] = useLocalStorage(
    'themeMapOutlineColor',
    '#00CBD9',
  );
  const [themeSnakeColor, setThemeSnakeColor] = useLocalStorage(
    'themeSnakeColor',
    '#111111',
  );
  const [themeSnakeOutlineColor, setThemeSnakeOutlineColor] = useLocalStorage(
    'themeSnakeOutlineColor',
    '#FF00FF',
  );
  const [themeAppleColor, setThemeAppleColor] = useLocalStorage(
    'themeAppleColor',
    '#F50E0E',
  );
  const [themeAppleOutlineColor, setThemeAppleOutlineColor] = useLocalStorage(
    'themeAppleOutlineColor',
    '#111111',
  );
  const [themeGoldenAppleColor, setThemeGoldenAppleColor] = useLocalStorage(
    'themeGoldenAppleColor',
    '#FFC821',
  );
  const [
    themeMapEffectSplashColor,
    setThemeMapEffectSplashColor,
  ] = useLocalStorage('themeMapEffectSplashColor', '#de0568');

  // Performance settings.
  const [performanceMode, setPerformanceMode] = useLocalStorage(
    'performanceMode',
    false,
  );

  // Arena settings.
  const [
    themeGoldenAppleOutlineColor,
    setThemeGoldenAppleOutlineColor,
  ] = useLocalStorage('themeGoldenAppleOutlineColor', '#111111');
  const [arenaWidth, setArenaWidth] = useLocalStorage(
    'arenaWidth',
    ARENA_DEFAULT_WIDTH,
  );
  const [arenaHeight, setArenaHeight] = useLocalStorage(
    'arenaHeight',
    ARENA_DEFAULT_HEIGHT,
  );

  const updateArenaWidth = value => {
    if (typeof value === 'function') {
      value = value(arenaWidth);
    }

    if (value <= ARENA_DEFAULT_WIDTH) {
      setArenaWidth(ARENA_DEFAULT_WIDTH);
    } else if (value > ARENA_MAXIMUM_WIDTH) {
      setArenaWidth(ARENA_MAXIMUM_WIDTH);
    } else {
      setArenaWidth(parseInt(value, 10) || ARENA_DEFAULT_WIDTH);
    }
  };

  const updateArenaHeight = value => {
    if (typeof value === 'function') {
      value = value(arenaHeight);
    }

    if (value <= ARENA_DEFAULT_HEIGHT) {
      setArenaHeight(ARENA_DEFAULT_HEIGHT);
    } else if (value > ARENA_MAXIMUM_HEIGHT) {
      setArenaHeight(ARENA_MAXIMUM_HEIGHT);
    } else {
      setArenaHeight(parseInt(value, 10) || ARENA_DEFAULT_HEIGHT);
    }
  };

  return (
    <SettingsProviderContext.Provider
      value={{
        theme: [theme, setTheme],
        themeBackgroundColor: [themeBackgroundColor, setThemeBackgroundColor],
        themeFontColor: [themeFontColor, setThemeFontColor],
        themeFontHoverColor: [themeFontHoverColor, setThemeFontHoverColor],
        themeMapColor: [themeMapColor, setThemeMapColor],
        themeMapOutlineColor: [themeMapOutlineColor, setThemeOutlineColor],
        themeSnakeColor: [themeSnakeColor, setThemeSnakeColor],
        themeSnakeOutlineColor: [
          themeSnakeOutlineColor,
          setThemeSnakeOutlineColor,
        ],
        themeAppleColor: [themeAppleColor, setThemeAppleColor],
        themeAppleOutlineColor: [
          themeAppleOutlineColor,
          setThemeAppleOutlineColor,
        ],
        themeGoldenAppleColor: [
          themeGoldenAppleColor,
          setThemeGoldenAppleColor,
        ],
        themeGoldenAppleOutlineColor: [
          themeGoldenAppleOutlineColor,
          setThemeGoldenAppleOutlineColor,
        ],
        themeMapEffectSplashColor: [
          themeMapEffectSplashColor,
          setThemeMapEffectSplashColor,
        ],
        performanceMode: [performanceMode, setPerformanceMode],
        arenaWidth: [arenaWidth, updateArenaWidth],
        arenaHeight: [arenaHeight, updateArenaHeight],
      }}
    >
      {children}
    </SettingsProviderContext.Provider>
  );
};

